This is covering a rendering problem on Ray Wenderlich Swift tutorial. The problem is on line 67-ish and adjusting the stuff so it adds to something else doesn't work by making the cube disapear. Any help would be greatly appreciated!!!

This is the line that has a problem:

memcpy(bufferPointer + MemoryLayout<Float>.size * Matrix4.numberOfElements(), projectionMatrix.raw(), MemoryLayout<Float>.size * Matrix4.numberOfElements())

The error is because of adding an unmutablepointer to an int. I tried adjusting the " + MemoryLayout<Float>.size * Matrix4.numberOfElements()" into different parts of the parameters. It doesn't help at all. The shaders I check were perfectly fine, so I doubt there would be  problems there.

